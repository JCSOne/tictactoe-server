package tictactoe.server;

import java.io.IOException;
import java.net.ServerSocket;

public class TicTacToeServerMedium
{

    private Game _game;
    private Thread _gameThread;
    private ServerSocket _listener;

    public void start()
    {
        try
        {
            _listener = new ServerSocket(5001);
            while (true)
            {
                _game = new Game(_listener.accept(), Dificulty.Medium);
                _gameThread = new Thread(_game);
                _gameThread.start();
            }
        }
        catch (IOException e)
        {
            System.out.println("There was an error: " + e.getMessage());
        }
        finally
        {
            try
            {
                _listener.close();
            }
            catch (IOException e)
            {
                System.out.println("There was an error closing the socket: "
                        + e.getMessage());
            }
        }
    }

    public static void main(String[] args) throws IOException
    {
        TicTacToeServerMedium server = new TicTacToeServerMedium();
        server.start();
    }
}
