package tictactoe.server;

public enum Dificulty
{
    Easy,
    Medium,
    Hard
}
