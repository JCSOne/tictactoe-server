package tictactoe.server;

import java.io.IOException;
import java.net.ServerSocket;

public class TicTacToeServerHard
{

    private Game _game;
    private Thread _gameThread;
    private ServerSocket _listener;

    public void start()
    {
        try
        {
            _listener = new ServerSocket(5002);
            while (true)
            {
                _game = new Game(_listener.accept(), Dificulty.Hard);
                _gameThread = new Thread(_game);
                _gameThread.start();
            }
        }
        catch (IOException e)
        {
            System.out.println("There was an error: " + e.getMessage());
        }
        finally
        {
            try
            {
                _listener.close();
            }
            catch (IOException e)
            {
                System.out.println("There was an error closing the socket: "
                        + e.getMessage());
            }
        }
    }

    public static void main(String[] args) throws IOException
    {
        TicTacToeServerHard server = new TicTacToeServerHard();
        server.start();
    }
}
