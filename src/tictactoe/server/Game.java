package tictactoe.server;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Random;

public class Game implements Runnable
{

    private char _computerMark;
    private char _playerMark;
    private char _board[]
            =
            {
                '1', '2', '3', '4', '5', '6', '7', '8', '9'
            };
    private Random _rand;
    private BufferedReader _reader;
    private PrintWriter _writer;
    private Socket _socket;
    private Dificulty _dificulty;

    public Game(Socket socket, Dificulty dicifulty)
    {
        System.out.println("Connection stablished");
        _rand = new Random();
        _socket = socket;
        _dificulty = dicifulty;
        try
        {
            _reader = new BufferedReader(new InputStreamReader(_socket.
                    getInputStream()));
            _writer = new PrintWriter(_socket.getOutputStream(), true);
        }
        catch (IOException e)
        {
            System.out.println("Connection couldn't be stablished: " + e.
                    getMessage());
        }
    }

    public void setPlayerMark(char mark)
    {
        _playerMark = mark;
        _computerMark = _playerMark == 'X' ? 'O' : 'X';
    }

    public int checkForWinner()
    {
        for (int i = 0; i <= 6; i += 3)
        {
            if (_board[i] == _playerMark && _board[i + 1] == _playerMark
                    && _board[i + 2] == _playerMark)
            {
                return 2;
            }
            if (_board[i] == _computerMark && _board[i + 1] == _computerMark
                    && _board[i + 2] == _computerMark)
            {
                return 3;
            }
        }

        for (int i = 0; i <= 2; i++)
        {
            if (_board[i] == _playerMark && _board[i + 3] == _playerMark
                    && _board[i + 6] == _playerMark)
            {
                return 2;
            }
            if (_board[i] == _computerMark && _board[i + 3] == _computerMark
                    && _board[i + 6] == _computerMark)
            {
                return 3;
            }
        }

        if ((_board[0] == _playerMark && _board[4] == _playerMark && _board[8]
             == _playerMark) || (_board[2] == _playerMark && _board[4]
                                 == _playerMark && _board[6] == _playerMark))
        {
            return 2;
        }
        if ((_board[0] == _computerMark && _board[4] == _computerMark
             && _board[8] == _computerMark) || (_board[2] == _computerMark
                                                && _board[4] == _computerMark
                                                && _board[6] == _computerMark))
        {
            return 3;
        }

        for (int i = 0; i < _board.length; i++)
        {
            if (_board[i] != _playerMark && _board[i] != _computerMark)
            {
                return 0;
            }
        }

        return 1;
    }

    public boolean setPlayerMove(int pos)
    {
        if (pos < 0 || pos > _board.length)
        {
            return false;
        }
        if (_board[pos] == _playerMark || _board[pos] == _computerMark)
        {
            return false;
        }
        _board[pos] = _playerMark;
        return true;
    }

    public int setComputerMove()
    {
        int move;

        if (_dificulty.equals(Dificulty.Hard))
        {
            for (int i = 0; i < _board.length; i++)
            {
                if (_board[i] != _playerMark && _board[i] != _computerMark)
                {
                    char curr = _board[i];
                    _board[i] = _computerMark;
                    if (checkForWinner() == 3)
                    {
                        return i;
                    }
                    else
                    {
                        _board[i] = curr;
                    }
                }
            }
        }

        if (_dificulty.equals(Dificulty.Hard) || _dificulty.equals(
                Dificulty.Medium))
        {
            for (int i = 0; i < _board.length; i++)
            {
                if (_board[i] != _playerMark && _board[i] != _computerMark)
                {
                    char curr = _board[i];   // Save the current number
                    _board[i] = _playerMark;
                    if (checkForWinner() == 2)
                    {
                        _board[i] = _computerMark;
                        return i;
                    }
                    else
                    {
                        _board[i] = curr;
                    }
                }
            }
        }

        do
        {
            move = _rand.nextInt(_board.length);
        }
        while (_board[move] == _playerMark || _board[move] == _computerMark);

        _board[move] = _computerMark;
        return move;
    }

    @Override
    public void run()
    {
        String message;
        int win = 0;
        try
        {
            while (win == 0)
            {
                message = _reader.readLine();
                if (message.toUpperCase().charAt(0) == 'X' || message.
                        toUpperCase().charAt(0) == 'O')
                {
                    setPlayerMark(message.toUpperCase().charAt(0));
                    _writer.println("playerTurn");
                }
                if (message.contains("move:"))
                {
                    if (setPlayerMove(Integer.parseInt(message.substring(5)) - 1))
                    {
                        win = checkForWinner();
                        if (win == 0)
                        {
                            int computerMove = setComputerMove();
                            win = checkForWinner();
                            _writer.println("computerMove:" + computerMove);
                        }
                    }
                    else
                    {
                        _writer.println("wrongMove");
                    }
                }
                displayBoard();
                switch (win)
                {
                    case 1:
                        _writer.println("draw");
                        break;
                    case 2:
                        _writer.println("youWon");
                        break;
                    case 3:
                        _writer.println("youLost");
                        break;
                    default:
                        _writer.println("error");
                        break;
                }
            }
        }
        catch (IOException ex)
        {
            // Ignored
        }
    }

    private void displayBoard()
    {
        System.out.println();
        System.out.println(_board[0] + " | " + _board[1] + " | " + _board[2]);
        System.out.println("-----------");
        System.out.println(_board[3] + " | " + _board[4] + " | " + _board[5]);
        System.out.println("-----------");
        System.out.println(_board[6] + " | " + _board[7] + " | " + _board[8]);
        System.out.println();
    }
}
