package tictactoe.server;

import java.io.IOException;
import java.net.ServerSocket;

public class TicTacToeServerEasy
{

    private Game _game;
    private Thread _gameThread;
    private ServerSocket _listener;

    public void start()
    {
        try
        {
            _listener = new ServerSocket(5000);
            while (true)
            {
                _game = new Game(_listener.accept(), Dificulty.Easy);
                _gameThread = new Thread(_game);
                _gameThread.start();
            }
        }
        catch (IOException e)
        {
            System.out.println("There was an error: " + e.getMessage());
        }
        finally
        {
            try
            {
                _listener.close();
            }
            catch (IOException e)
            {
                System.out.println("There was an error closing the socket: "
                        + e.getMessage());
            }
        }
    }

    public static void main(String[] args) throws IOException
    {
        TicTacToeServerEasy server = new TicTacToeServerEasy();
        server.start();
    }
}
